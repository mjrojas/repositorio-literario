Poema “Ensayo sobre las cosas en las que más pienso”,
de Anne Carson

 

 

En el error.

Y en sus emociones.

Estar a punto del error es una condición del miedo.

Estar en medio del error es estar en un estado de locura y de derrota.

Darte cuenta de que has cometido un error produce vergüenza y remordimiento.

¿O sí?

 

Veamos.

Mucha gente, incluyendo a Aristóteles, opina que el error

es un suceso mental interesante y valioso.

Cuando habla de la metáfora en su Retórica,

Aristóteles dice que hay tres tipos de palabras:

las extrañas, las ordinarias y las metafóricas.

 

“Las palabras extrañas simplemente nos descolocan;

las palabras ordinarias nos transmiten lo que ya sabíamos;

usando metáforas es como nos topamos con lo nuevo y con lo fresco”

(Retórica, 1410b10-15).

¿En qué consiste esa frescura de las metáforas?

Aristóteles dice que la metáfora hace que la mente se experimente a sí misma

 

en el acto de cometer un error.

Ve la mente como algo que se mueve a lo largo de una superficie plana

hecha de lenguaje ordinario

y luego de repente

esa superficie se rompe o se complica.

Emerge lo inesperado.

 

Al principio parece raro, contradictorio o incorrecto.

Y después sí tiene sentido.

Y es en ese momento cuando, según Aristóteles,

la mente se dirige a sí misma y se dice:

“¡Qué verdad es! ¡Y aun así cómo lo había malinterpretado yo todo!”

Es posible aprender una lección de los errores auténticos de las metáforas.

 

No es solo que las cosas no son lo que parecen,

y de ahí que nos confundamos;

además, dicha equivocación es en sí valiosa.

Pero esperad un momento, dice Aristóteles,

hay mucho que ver y sentir por ahí.

Las metáforas le enseñan a la mente

 

a disfrutar del error

y a aprender

de la yuxtaposición entre lo que es y lo que no es.

Hay un proverbio chino que dice:

un pincel no puede escribir dos caracteres en la misma pincelada.

Y aun así

 

eso es justamente lo que hace un buen error.

Veamos un ejemplo.

Es un fragmento de cierto poema griego antiguo

que contiene un error de aritmética.

Por lo visto el poeta no sabe

que 2+2=4.

 

Fragmento Alkman 20:

[?] lo cual hacen tres estaciones, verano

e invierno y en tercer lugar otoño

y en cuarto lugar primavera cuando

hay florecimientos pero comer suficiente

no lo es.

 

Alkman vivió en Esparta en el s. VII a.C.

Entonces Esparta era un estado pobre

y es improbable

que Alkman llevara una vida de rico bien alimentado.

Este hecho es el contexto de sus observaciones

que desembocan en el hambre.

 

Siempre tenemos la sensación de que el hambre

es un error.

Alkman hace que experimentemos este error

con él

mediante un uso efectivo del error computacional.

Para un poeta espartano pobre sin nada

 

en sus bolsillos

al final del invierno,

ahí viene la primavera

como una ocurrencia a deshora de la economía natural,

la cuarta en una serie de tres,

desequilibrada su aritmética

y su verso yámbico.

 

El poema de Alkman se parte en dos a mitad camino con ese metro yámbico

sin dar ninguna explicación

sobre de dónde viene la primavera

o sobre por qué los números no nos ayudan

a controlar mejor la realidad.

 

Tres cosas me gustan del poema de Alkman.

Primero, que es pequeño,

ligero

y económico de una manera más que perfecta.

Segundo, que parece sugerir la presencia de ciertos colores como el verde pálido

sin ni siquiera nombrarlos.

 

Tercero, que consigue sacar a relucir

algunas preguntas metafísicas de primer orden

(como la de Quién hizo el mundo)

sin un análisis excesivo.

Fijémonos en que en el predicado verbal “lo cual hacen” en el primer verso

no hay sujeto: [?]

 

Es muy poco habitual en griego

que el predicado verbal no tenga sujeto; de hecho,

es un error gramatical.

Si les preguntáis, los filólogos estrictos os dirán

que este error no es más que un accidente de transmisión,

que el poema tal y como nos ha llegado

con toda seguridad es un fragmento suelto

de un texto más extenso

y que es casi seguro que Alkman nombró

al agente de la creación

en los versos precedentes.

Bueno, puede ser.

 

Pero, como sabéis, el principal objetivo de la filología

es reducir todo placer textual

a un mero accidente histórico.

Y no me siento cómoda con la idea de que podemos saber exactamente

qué es lo que quiere decir el poeta.

Por lo tanto, dejemos el interrogante aquí

 

al comienzo del poema

y admiremos la valentía de Alkman

a la hora de confrontar aquello que queda entre paréntesis.

La cuarta cosa que me gusta

del poema de Alkman

es la impresión que da

 

de hacer que se desembuche la verdad, en contra de sí misma.

Muchos poetas aspiran

a conseguir este tono de lucidez inadvertida

pero pocos se dan cuenta tan fácilmente como Alkman.

Por supuesto, su simplicidad es un fake.

Alkman no es para nada simple,

es un maestro de la organización

(o lo que Aristóteles llamaría un “imitador”

de la realidad).

La imitación (mímesis, en griego)

es el término que utiliza Aristóteles para designar a los errores auténticos de la poesía.

Lo que me gusta de este término

 

es la facilidad con la que admite

que aquello con lo que nos las vemos cuando hacemos poesía es el error,

la obstinada creación del error,

el rompimiento deliberado y la complicación de los errores

de los cuales puede emerger

lo inesperado.

 

Así que un poeta como Alkman

deja a un lado el miedo, la ansiedad, la vergüenza, el remordimiento

y el resto de emociones tontas que asociamos con el hecho de cometer errores

para aceptar

la verdad verdadera.

La verdad verdadera en el caso de los humanos es la imperfección.

 

Alkman rompe con las reglas de la aritmética

y hace peligrar la gramática

y no da pie con bola en cuanto a la forma métrica de sus versos

para llevarnos a aceptar este hecho.

Al final del poema el hecho sigue ahí

y probablemente Alkman no tiene menos hambre.

 

Sin embargo, algo ha cambiado en el coeficiente de nuestras expectativas.

Porque, haciendo que nos equivocáramos,

Alkman ha perfeccionado algo.

Sí, ha mejorado algo, ha mejorado algo de una manera

más que perfecta.

Con un solo pincel.

 

 
